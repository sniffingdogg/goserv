package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	port := 9599

	http.HandleFunc("/sniffingdogg", doggHandler)

	log.Printf("Server starting on port %v\n", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", port), nil))
}

func doggHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "sniffingdogg in da his-ouse\n")
}
